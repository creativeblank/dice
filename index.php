<!doctype html>
<html lang='en-GB'>
<head>
	<meta charset='utf-8' />
	<title></title>
	<link rel="stylesheet" type="text/css" href="./css/reset.css" />
	<link rel="stylesheet" type="text/css" href="./css/style.css" />
	<link href="http://fonts.googleapis.com/css?family=Quicksand:300" rel="stylesheet" type="text/css">
</head>
<body>

<div class='wrap'>
	<section class='dice_wrapper'>

		<div class='die d4'>
			<h2>d4</h2>
		</div>
		<div class='die d6'>
			<h2>d6</h2>
		</div>
		<div class='die d8'>
			<h2>d8</h2>
		</div>
		<div class='die d10'>
			<h2>d10</h2>
		</div>
		<div class='die d12'>
			<h2>d12</h2>
		</div>
		<div class='die d20'>
			<h2>d20</h2>
		</div>

	</section>
	<div class='selected_dice'>
		<ul class='dice_list'>

		</ul>
	</div>
	<section class='modifier'>
		<h3>+</h3>
		<form>
			<input type='text' name='modifier' placeholder='modifier' maxlength='2' />
			<input type='button' name='roll' value='roll!' />
		</form>	
	</section>
	<section class='results'>
		<article class='result'>
			<h4></h4>
			<ul class='secret_maths'></ul>
		</article>
	</section>
</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="./js/dice.js"></script>
</html>