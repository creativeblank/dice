$(document).ready(function() {
	selected = [];
	rolled = [];
	result = 0;

	$('.die').click(function() {
		var clicked = $(this).children('h2').html();
		var die_array = clicked.split('d');
		var die_value = die_array[1];
		selected.push(die_value);
		$('.dice_list').prepend('<li>'+clicked+'</li>');
	});

	$('input[name="roll"]').click(function() {
		rolled = [];
		result = 0;
		results = [];
		$('.secret_maths').html('');
		if (selected.length > 0) {
			var modifier = $('input[name="modifier"]').val();
			if (!modifier)
				modifier = 0;
			if (!is_numeric(modifier))
				modifier = 0;
			$.each(selected, function(index, value) {
				var troll = roll(value);
				rolled.push(troll);
			});
			$.each(rolled, function(index, value) {
				result = result+value;
				results.push(value);
			});
			result = parseInt(result) + parseInt(modifier);
			if (modifier !== 0)
				results.push(parseInt(modifier));
			$.each(results, function(index, value) {
				$('.secret_maths').prepend('<li>'+value+'</li>');
			});
			$('input[name="modifier"]').val(null);
			selected = [];
			$('.dice_list').html('');
			$('.result h4').html(result + '!');
		}
		else {
			$('.result h4').html('error!');
		}
	});

});

function roll(sides) {
	var result = Math.floor((Math.random()*sides)+1);
	return result;
}

function is_numeric(test) {
	return (test >= 0 || test <= 0);
}